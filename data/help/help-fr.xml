<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://docbook.org/xml/4.2/docbookx.dtd">
<article lang="fr">
<title>Manuel de PyMecaVideo</title>
<articleinfo>
<author>
<firstname>Georges</firstname>
<surname>Khaznadar</surname>
<address><email>georgesk@debian.org</email></address>
</author>
<date>1er janvier 200826 octobre 2010</date>
<abstract>
<para>
PyMecaVideo est un logiciel facilitant le repérage de points mobiles dans
une vidéo et d'en exploiter la cinématique pour des usages de mesure physique.
</para>
</abstract>
</articleinfo>
<sect1>
<title>PyMecaVideo, pourquoi faire&nbsp;?</title>
<mediaobject><imageobject><imagedata fileref="snap1.png" align="left"></imagedata></imageobject></mediaobject>
<para>Il est possible de faire des mesures physiques à l'aide d'images,
dans certaines conditions. <symbol> PyMecaVideo</symbol> facilite
les mesures physiques faites sur des séquences d'images vidéo. Il suffit de
travailler à partir d'un fichier vidéo
de type <computeroutput>AVI</computeroutput>,
encodé à l'aide d'un codec libre<footnote>
<para>Attention, un grand nombre de séquences vidéo sont encodées à l'aide
de logiciels de codage/décodage (codecs) propriétaires, dont les concepteurs
interdisent d'étudier le fonctionnement. <symbol> PyMecaVideo</symbol>
est un logiciel libre, il n'incorpore pas de sous-ensemble opaque,
donc il vous appartient de vérifier la codec utilisé par les séquences vidéo
que vous voulez utiliser.</para>
<para>Attention : le format <computeroutput>.avi</computeroutput> du fichier
est un format conteneur, qui autorise différents codecs. On ne peut donc
pas se baser sur le nom complet du fichier pour deviner le codec de celui-ci.
Pour déterminer le codec d'un fichier vidéo, vous pouvez utiliser la commande
<computeroutput>file</computeroutput>, par exemple&nbsp;:
<programlisting>$ file video/g1.avi
video/g1.avi: RIFF (little-endian) data, AVI, 400 x 300, 25.00 fps, video: DivX 5
</programlisting>
</para>
</footnote>, 
qui comporte au moins une image dont on peut
déterminer l'échelle.
</para>
<para><symbol> PyMecaVideo</symbol> permet d'ouvrir et de décomposer
en images la vidéo, et par des pointages successifs, de suivre le mouvement 
d'un point, et éventuellement le mouvement de l'origine d'un référentiel
mobile dans le cadre des images. Les données pointées sont ensuite exploitables
à l'aide de logiciels d'analyse standard.
</para>
</sect1>
<sect1>
<title>Téléchargement et installation</title>

<para>
<ulink url="http://outilsphysiques.tuxfamily.org/pmwiki.php/Oppl/Pymecavideo">
<medialabel><inlinemediaobject><imageobject> <imagedata fileref="snap2-mini.png" align="left"></imagedata></imageobject></inlinemediaobject></medialabel>
</ulink>  Les sources de <symbol> PyMecaVideo</symbol> sont disponibles parmi
<ulink url="http://outilsphysiques.tuxfamily.org/pmwiki.php/Oppl/Pymecavideo">
les &laquo;&nbsp;Outils Physiques Pour Linux&nbsp;&raquo; hébergés sur le
site <emphasis>tuxfamily.org</emphasis></ulink>. Pour l'installation sous
<productname> Linux</productname>, récupérer l'arbre des sources comme
expliqué à 
<ulink url="http://outilsphysiques.tuxfamily.org/pmwiki.php/Oppl/Pymecavideo">
la page d'accueil du projet</ulink>, entrer dans le
répertoire <computeroutput>pymecavideo/trunk</computeroutput>, et taper la
commande suivante : <computeroutput>python setup.py install</computeroutput>.
Cela met en place les modules nécessaires pour python. 
Activer l'attribut exécutable du fichier 
<computeroutput>pymecavideo</computeroutput>, par exemple en tapant la
commande <computeroutput>chmod +x pymecavideo</computeroutput>, puis copier ce
fichier dans une place où celui-ci sera facile à lancer en tant que commande.
La liste des répertoires accessibles pour lancer une commande peut être
obtenue en tapant <computeroutput>echo $PATH</computeroutput>.
</para>
<para>
Il est possible aussi d'obtenir des fichiers binaires, qui sont pris en charge
par le gestionnaire de paquets de votre distribution préférée. Actuellement
on trouve des paquets RPM et DEB.
</para>
<para>Les paquets pour Debian et Ubuntu sont disponibles dans les dépôts
officiels de ces distributions.
</para>
</sect1>
<sect1>
  <title>Lancement de PyMecaVideo</title>
<mediaobject><imageobject><imagedata fileref="snap3-mini.png" align="left"></imagedata></imageobject></mediaobject>
  <para>
    Il est possible de lancer <symbol>PyMecaVideo</symbol> de diverses
    façons : en ligne, on peut taper 
    <computeroutput>pymecavideo</computeroutput>
    ou <computeroutput>pymecavideo nom_de_fichier_video.avi</computeroutput>.
    On peut aussi le lancer à partir du menu graphique de votre gestionnaire
    de fenêtre : <symbol>KDE</symbol> et <symbol>Gnome</symbol> intègrent
    l'application dans le menu si vous faites une installation à l'aide d'un
    paquet. 
    <variablelist>
      <varlistentry><term>Dans le cas de KDE,</term>
      <listitem><para>l'application devrait être accessible 
      par les menus 
      Éducatif&nbsp;-&gt;&nbsp;Science&nbsp;-&gt;&nbsp;PyMecaVideo.
    </para>
      </listitem>
      </varlistentry>
      <varlistentry><term>Dans le cas de Gnome,</term>
      <listitem><para>l'application devrait être accessible 
      par les menus 
      Éducation&nbsp;-&gt;&nbsp;PyMecaVideo.
    </para>
      </listitem>
      </varlistentry>
    </variablelist>
  </para>
  <para>
    <ulink url="/usr/share/icons/pymecavideo.svg">
    Voici l'icône de l'application&nbsp;:
    <inlinemediaobject><imageobject><imagedata fileref="/usr/share/icons/pymecavideo.png" valign="middle" width="2em"></imagedata></imageobject></inlinemediaobject>
    </ulink>
  </para>
</sect1>

<sect1><title>Préparer une vidéo pour pouvoir la traiter</title>
<para>
La préparation de la vidéo commence dès la prise de vues. Les points-clés
sont les suivants&nbsp;:
<itemizedlist>
  <listitem><para>
    Choisir un bon éclairage. Les éclairages fluorescents posent parfois
    problème, car leur intensité varie très rapidement, et cela peut donner
    quelquefois naissance à des phénomènes stroboscopiques. D'autre part, pour
    les caméras vidéo disposant de bons automatismes, un éclairage suffisant
    facilite le choix de temps de pose courts, qui sont favorables à
    l'acquisition de données.
  </para></listitem>
  <listitem><para>
    La scène doit posséder un bon contraste. En particulier les points à
    suivre doivent se distinguer aisément par valeur, sinon par la couleur, 
    de leur environnement.
  </para></listitem>
  <listitem><para>
    Il faut penser à laisser un objet de taille connue, dans le même plan
    focal que la scène qui sera filmée. Il est bon de documenter la dimension
    de l'objet-étalon.
  </para></listitem>
  <listitem><para>
    L'axe optique de la caméra doit être perpendiculaire au plan dans lequel
    se déplace l'objet étudié. Il doit pointer environ vers le milieu de
    la trajectoire probable.
  </para></listitem>
  <listitem><para>
    On commence à filmer avant l'évènement, on arrête après l'évènement.
    Il est préférable de fixer le début et la fin de la séquence vidéo
    à l'étape de montage.
  </para></listitem>
  <listitem><para>
    Utiliser un logiciel libre de montage, et couper la séquence pertinente.
    Quelques dizaines d'images au plus suffisent. La choix d'un logiciel
    libre assure que les codecs utilisés ne poseront pas de problème à
    l'avenir. Exemples de logiciels utilisables : 
    <ulink url="http://fr.wikipedia.org/wiki/Kino_(logiciel)">Kino</ulink>, 
    <ulink url="http://fr.wikipedia.org/wiki/Cinelerra">Cinelerra</ulink>.
    La vidéo sera enregistrée dans un conteneur au format 
    <computeroutput>.avi</computeroutput>.
  </para></listitem>
</itemizedlist>
</para>
</sect1>

<sect1><title>Utilisation de PyMecaVideo</title>
<mediaobject><imageobject><imagedata fileref="snap4.png" align="left"></imagedata></imageobject></mediaobject>
<para>
Il faut bien sûr commencer par charger une vidéo avant de pouvoir en faire 
quoi que ce soit. Ça peut se faire au lancement de la commande, en tapant
en ligne de commande&nbsp;:
<computeroutput>pymecavideo nom_de_fichier_video.avi</computeroutput>, ou
en mode graphique quand <symbol>PyMecaVideo</symbol> est lancé, par le
menu Fichier&nbsp;-&gt;&nbsp;Ouvrir une vidéo, ou encore si on veut une
des vidéos d'exemple, par le menu Aide&nbsp;-&gt;&nbsp;Exemples ...
</para>
<sect2><title>Régler l'échelle</title>

<para>Quand on dispose d'un fichier vidéo ouvert, le bouton 
&laquo;&nbsp;Définir l'échelle&nbsp;&raquo; est actif, mais la valeur de 
l'échelle est encore indéfinie. On peut utiliser le curseur ou le champ de
numéro d'image pour faire apparaître une image autre que la première
de la séquence vidéo. Il faut que l'objet étalon soit dans le champ, et il
faut connaître sa longueur en mètre.
</para>
<para>
On clique alors sur ce bouton, on renseigne la longueur de l'objet-étalon
dans le dialogue qui surgit (on utilise la virgule ou le point 
comme séparateur décimal, indifféremment&nbsp;:
par exemple 0,60 m pour signifier 60 cm), on valide, puis lorsque
le curseur de la souris devient une crois de Malte, on réalise un tirer-glisser
d'une extrémité à l'autre de l'objet-étalon. 
</para>
<mediaobject><imageobject><imagedata fileref="snap7.png" align="left"></imagedata></imageobject></mediaobject>
<para>
Quand l'échelle est définie, c'est pour toute l'acquisition de données.
En cas d'erreur, on peut tout réinitialiser à l'aide du bouton
&laquo;&nbsp;Tout réinitialiser&nbsp;&raquo;
</para>
</sect2>
<sect2><title>Choix du nombre de points à étudier</title>
<para>Par défaut, on étudie le mouvement d'un seul point, le référentiel
étant celui de la caméra.
</para>
<mediaobject><imageobject><imagedata fileref="snap8.png" align="left"></imagedata></imageobject></mediaobject>
<para>Il est possible de faire porter l'étude sur deux points ou plus.
Un des points de la série pourra plus tard être utilisé comme nouvelle origine
du référentiel à volonté.
</para>
<para>Pour étudier plus d'un point, il faut modifier la valeur affichée dans
la boîte intitulée &laquo;&nbsp;Nombre de points à étudier&nbsp;&raquo;
</para>
</sect2>
<sect2><title>Pointer les positions</title>
<mediaobject><imageobject><imagedata fileref="snap6.png" align="left"></imagedata></imageobject></mediaobject>
<para>
  On peut décider de ne pas commencer le pointage dès la première image. 
  Dans ce cas, il faut choisir manuellement l'image initiale à l'aide du 
  curseur en haut à gauche, ou du champ de sélection de numéro d'image.
  Le pointage commence quand on clique sur le bouton
  &laquo;&nbsp;Démarrer l'acquisition&nbsp;&raquo;. 
</para>
<mediaobject><imageobject><imagedata fileref="snap9.png" align="left"></imagedata></imageobject></mediaobject>
<para>Quand l'acquisition est en cours, une petite phrase au-dessus de la 
vidéo rappelle le numéro du point qu'on est censé cliquer en suivant. Les traces
des points cliqués dans les images précédentes de la vidéo apparaissent en 
couleurs. Les couleurs doivent se suivre sans discontinuité, sinon c'est la
marque d'une faute de pointage.
</para>
<sect3><title>Défaire et refaire</title>
<mediaobject><imageobject><imagedata fileref="snap10.png" align="left"></imagedata></imageobject></mediaobject>
<para>Les icônes  &laquo;&nbsp;défaire&nbsp;&raquo; et 
&laquo;&nbsp;refaire&nbsp;&raquo; permettent éventuellement de rattraper
une faute de pointage quand celle-ci est reconnue.
</para>
<para>Il n'est possible de refaire des pointages antérieurs que tant qu'on n'a
apporté aucune modification aux pointages précédemment défaits. Dès qu'un
des pointages est corrigé à l'aide de la souris sur la vidéo, les pointages
suivants disparaissent de la liste à refaire, et le bouton 
&laquo;&nbsp;refaire&nbsp;&raquo; est aussitôt inactivé (grisé).
</para>
<para>
  Il est possible d'arrêter le pointage à tout moment, éventuellement avant 
  la fin de la séquence vidéo.
</para>
</sect3>
</sect2>
<sect2><title>Enregistrer le travail</title>
<para>
Après que des points aient été repérés, il devient possible d'enregistrer 
le travail grâce au menu Fichier&nbsp;-&gt;&nbsp;Enregistrer les données.
On choisit alors un fichier de type <computeroutput>.csv</computeroutput>,
<computeroutput>.dat</computeroutput>,<computeroutput>.txt</computeroutput>
ou <computeroutput>.asc</computeroutput><footnote><para>
Les types de fichiers <computeroutput>.csv</computeroutput>,
<computeroutput>.dat</computeroutput>,<computeroutput>.txt</computeroutput>
ou <computeroutput>.asc</computeroutput> sont par
convention réservés à des fichiers de données à un format texte pur, c'est
à dire humainement lisible. Essayez d'ouvrir un fichier que vous produirez
ainsi à l'aide d'un éditeur de texte, vous vous rendrez compte de sa 
structure, claire et simple à comprendre.</para></footnote>
</para>
<para><emphasis>NB&nbsp;:</emphasis>en même temps que le fichier que vous 
choisissez est enregistré, un autre le sera automatiquement. Si vous choisissez
par exemple d'enregistrer un fichier de nom 
<computeroutput>data.csv</computeroutput>, un autre fichier de nom
<computeroutput>data.csv.mecavideo</computeroutput> sera enregistré.
Voici les caractéristiques de ces deux fichiers&nbsp;:
<itemizedlist>
<listitem><para>
  Le fichier choisi explicitement (de nom <computeroutput>data.csv</computeroutput>
  par exemple) contient les données en unités seconde pour le temps et mètre
  pour les distances. C'est ce fichier qui est pertinent pour un traitement
  des données physiques à l'aide de logiciels d'analyse.
</para></listitem>
<listitem><para>
  Le fichier créé en plus (de nom <computeroutput>data.csv.mecavideo</computeroutput>
  pour l'exemple) contient des données différentes : ce sont uniquement 
  des nombres entiers pour les positions, c'est à dire qu'on enregistre les
  lieux des pointages en unité pixel. De plus, les données permettant de
  reconstituer l'échelle font partie de ce fichier. Ce dernier fichier est
  plutôt destiné à l'usage interne de l'application.
</para></listitem>
</itemizedlist>
</para>
</sect2>
<sect2><title>Le volet des trajectoires</title>
<mediaobject><imageobject><imagedata fileref="snap12.png" align="left"></imagedata></imageobject></mediaobject>
<para>Quand les pointages sont faits, il est intéressant d'activer le 
volet des trajectoires. Celui-ci permet d'attirer l'attention plus précisément
sur le mouvement et la vitesse des objets étudiés, en faisant apparaître 
ceux-ci sans la vidéo où on les avait capturés.
</para>
<sect3><title>Représentation absolue ou relative</title>
<mediaobject><imageobject><imagedata fileref="snap11.png" align="left"></imagedata></imageobject></mediaobject>
<para>Sitôt que plus d'un point fait partie de l'étude, il est possible de
faire afficher les positions et les vitesses de plusieurs façons&nbsp;:
soit les positions et les vitesses &laquo;&nbsp;absolues&nbsp;&raquo;, 
c'est-à-dire relatives au référentiel de la caméra, soit les positions 
et les vitesses &laquo;&nbsp;relatives&nbsp;&raquo; par rapport à un 
des points étudiés, choisi comme origine d'un nouveau référentiel.
</para>
</sect3>
<sect3><title>L'échelle pour les vecteurs vitesse</title>
<mediaobject><imageobject><imagedata fileref="snap13.png" align="left"></imagedata></imageobject></mediaobject>
<para>L'échelle des vecteurs vitesse se règle à l'aide d'une zone de saisie
intitulée &laquo;&nbsp;Échelle de vitesses&nbsp;&raquo;. Modifiez sa valeur
et appuyez sur la touche Entrée pour obtenir une autre taille des vecteurs
vitesse.
</para>
</sect3>
</sect2>
<sect2><title>Les préférences de PyMecaVideo</title>
<mediaobject><imageobject><imagedata fileref="snap14.png" align="left"></imagedata></imageobject></mediaobject>
<para>Par le menu &laquo;&nbsp;Édition -&gt; Préférences&nbsp;&raquo; ont
fait apparaître un menu pour les valeurs préférées, qui seront réutilisées
lors de lancements successifs de PyMecaVideo. Parmi ces préférences figurent
<itemizedlist>
<listitem><para>L'échelle des vitesses&nbsp;;
</para></listitem>
<listitem><para>La façon d'afficher les vecteurs vitesse&nbsp;: soit partout,
soit au voisinage du pointeur de souris quand on l'approche des points&nbsp;;
</para></listitem>
<listitem><para>L'afficheur vidéo à utiliser pour visualiser les vidéos de
synthèse&nbsp;: vlc, xine ou mplayer ... tous des visionneurs que 
Pymecavideo peut
contrôler par une ligne de commande&nbsp;;
</para></listitem>
<listitem><para>Le niveau de verbosité pour le débogage&nbsp; si un comportement
anormal de PyMecaVideo vous gène, augmenter la valeur de ce niveau permet 
éventuellement d'intercepter des messages informatifs au sujet du déroulement 
du programme. Ces messages peuvent éventuellement servir pour prendre
contact utilement avec les auteurs de PyMecaVideo et signaler un 
dysfonctionnement.
</para></listitem>
</itemizedlist>
</para>
</sect2>
<sect2><title>Créer une vidéo de synthèse</title>
<mediaobject><imageobject><imagedata fileref="snap15.png" align="left"></imagedata></imageobject></mediaobject>
<para>Dans le volet des trajectoire, on trouve deux boutons commandant
la fabrication d'une vidéo &laquo;&nbsp;de synthèse&nbsp;&raquo;, qui
permet de revoir le film tel qu'il aurait été si la caméra avait été
solidaire du point sélectionné comme origine du référentiel.
</para>
<para>Le bouton du dessous (Vidéo normale .. par défaut) sert à spécifier
un déroulement normal pour la vidéo de synthèse, ou un ralenti par un facteur
numérique de 2, 4 ou 8. Le bouton du dessus 
&laquo;&nbsp;Vidéo calculée&nbsp;&raquo;
déclenche un calcul assez long,
la génération de la vidéo de synthèse. Dès que cette vidéo est calculée, on
la voit apparaître en boucle dans une nouvelle fenêtre. Le logiciel utilisé
pour afficher cette vidéo est vlc, xine ou mplayer selon le choix réalisé dans
les préférences de PyMecaVideo.
</para>
<sect3><title>Exemple de vidéo de synthèse</title>
<para>Les vidéos de synthèse sont intéressantes quand on choisit comme 
origine de référentiel un point qui se déplace en ligne droite à 
vitesse constante.
</para>
<para>Par exemple, on peut étudier simultanément deux points, dans une vidéo
d'un cycliste laissant tomber un objet&nbsp;: un point du cadre du vélo
(P<subscript>1</subscript>) et un point de l'objet qui tombe 
(P<subscript>2</subscript>). Dans le référentiel terrestre, celui de la caméra
si on est en plan fixe, la trajectoire de P<subscript>2</subscript> est une
parabole. Mais dans le référentiel galiléen d'origine P<subscript>1</subscript>,
le point P<subscript>2</subscript> est en chute libre sans vitesse initiale.
</para>
<para>Dans ces circonstances, la vidéo de synthèse obtenue après avoir choisi 
de représenter le volet des trajectoires à partir de l'origine 
P<subscript>1</subscript> montre bien une chute libre.
</para>
<para><emphasis>NB.</emphasis>&nbsp;: quand on choisit de réaliser une vidéo
de synthèse, il faut garder présent à l'esprit que les images seront en fait
découpées à l'intérieur des images déjà existantes. Il faut donc éviter de
traiter des images où le point qui utilisé comme origine se trouve 
trop près d'un bord de l'image, parce que ça diminue la taille du 
gabarit de découpe utilisé pour produire la vidéo de
synthèse.
</para>
<!-- ATTENTION : IL FAUDRAIT ILLUSTRER ÇA AVEC DES IMAGES !!! -->
</sect3>
</sect2>
<sect2><title>Le volet des coordonnées</title>
<mediaobject><imageobject><imagedata fileref="snap16.png" align="left"></imagedata></imageobject></mediaobject>
<para>Le troisième et dernier volet de PyMecaVideo est le volet des
coordonnées. Il présente un tableau dont la première colonne représente des
dates en seconde, et les suivantes des coordonnées en mètre des points étudiés.
Ce tableau est juste là pour démystifier le fonctionnement de PyMecaVideo,
montrer qu'il ne s'agit que de traitement numérique.
</para>
<para>Cependant, ce volet des coordonnées est le lieu utile pour envoyer les
données de PyMecaVideo vers d'autres applications qui serviront à les analyser.
</para>
</sect2>
<sect2><title>Exporter des données vers d'autres applications</title>
<mediaobject><imageobject><imagedata fileref="snap17.png" align="left"></imagedata></imageobject></mediaobject>
<para>Le bouton 
&laquo;&nbsp;Copier les données vers le presse-papiers&nbsp;&raquo; permet
d'exporter la totalité du tableau vers le presse-papiers. De même, il suffit de
sélectionner (de mettre en surbrillance) une partie du tableau pour que 
cette partie soit automatiquement copiée vers le presse-papiers. La partie
sélectionnée peut aussi être tirée-glissée vers d'autres applications. Si
ces applications sont capables de récupérer les données et d'en faire usage,
elles accepteront l'exportation. C'est le cas des traitements de texte et
des tableurs les plus courants.
</para>
</sect2>
</sect1>

<sect1><title>Les logiciels de traitement des données</title>
<para>
<mediaobject><imageobject><imagedata fileref="snap16.png" align="left"></imagedata></imageobject></mediaobject>
Le volet « coordonnées » de Pymecavideo permet des exportation automatiques
vers divers logiciels de traitement de données, en plus du simple export
vers le presse-papiers. Les exportations automatiques permettent l'ouverture
des logiciels choisis, avec les données prêtes à l'utilisation. Le tableau 
suivant signale les logiciels supportés, ainsi que leur disponibilité sous
GNU/Linux et sous <trademark>Windows</trademark>.
<table frame='all'><title>Logiciels libres de traitement des données</title>
<tgroup cols='4' align='center' colsep='1' rowsep='1'>
<colspec colname='Nom logiciel'/>
<colspec colname='Linux'/>
<colspec colname='Windows'/>
<colspec colname='commentaire'/>
<thead>
  <row>
  <entry>Logiciel</entry>
  <entry><inlinemediaobject><imageobject>
    <imagedata fileref="tux-w32.png" format="PNG" width="32px" />
  </imageobject></inlinemediaobject></entry>
  <entry><inlinemediaobject><imageobject>
    <imagedata fileref="win-w32.png" format="PNG" width="32px" />
  </imageobject></inlinemediaobject></entry>
  <entry>Description</entry>
</row>
</thead>
<tbody>
<row>
  <entry>OpenOffice.org <emphasis role="strong">
  <ulink url="http://fr.openoffice.org/docs/Calc.html">Calc</ulink>
  </emphasis></entry>
  <entry><emphasis role="strong">&times;</emphasis></entry>
  <entry><emphasis role="strong">&times;</emphasis></entry>
  <entry>
    Un tableur plutôt destiné à la bureautique, mais déjà connu par 
    de nombreuses personnes
  </entry>
</row>
<row>
  <entry><emphasis role="strong">
    <ulink url="http://soft.proindependent.com/qtiplot_fr.html">Qtiplot</ulink>
  </emphasis></entry>
  <entry><emphasis role="strong">&times;</emphasis></entry>
  <entry><emphasis role="strong">&nbsp;</emphasis></entry>
  <entry>
    Un véritable outil de traitement de données, très riche en fonctionnalités
  </entry>
</row>
<row>
  <entry><emphasis role="strong">
    <ulink url="http://scidavis.sourceforge.net/"> SciDavis</ulink>
  </emphasis></entry>
  <entry><emphasis role="strong">&times;</emphasis></entry>
  <entry><emphasis role="strong">&nbsp;</emphasis></entry>
  <entry>
    Un autre outil de traitement de données, aussi riche en fonctionnalités
  </entry>
</row>
</tbody>
</tgroup>
</table>
</para>
</sect1>

<sect1><title>Comment contribuer</title>
<para>
<itemizedlist>
<listitem><para>
  <emphasis role="strong">En rapportant les bogues</emphasis> 
  aux auteurs. Celles-ci peuvent être de 
  diverses natures : disfonction du logiciel, mauvaise présentation de 
  l'interface, souhait d'amélioration, etc. </para>
  <para>Dans chaque cas, soyez très précis : un rapport de bogue ne permet
  à l'auteur de réagir que si celui-ci donne une description détaillée.
  Aussi, n'hésitez pas à jeter un coup d'œil dans les sources du logiciel :
  vous les avez à disposition ! en fait, plus d'un contributeur s'est déjà
  joint aux auteurs, à l'occasion d'un rapport de bogue, pour lequel il
  apportait la solution (une modification de la source).</para>
  <para>Les sources de Pymecavideo sont en langage Python, et en général 
  assez bien commentées pour être largement compréhensibles.
</para></listitem>
<listitem><para>
  Pubiez des 
  <emphasis role="strong">vidéos pédagogiques sous licences libres</emphasis> !
  Les vidéos déjà
  utilisables ne manquent pas, mais beaucoup d'entre elles sont publiées
  sans que leur licence permette vraiment de les réutiliser librement.</para>
  <para>Notez bien qu'une vidéo sans licence, en droit français, appartient
  à son auteur et ne peut pas être redistribuée sans son consentement écrit.
</para></listitem>
<listitem><para>
  Une liste de diffusion permet d'échanger des idées de façon constructive,
  abonnez vous à <ulink url="mailto:pymecavideo-request@lists.tuxfamily.org?subject=subscribe"><email>pymecavideo@lists.tuxfamily.org</email></ulink>
</para></listitem>
</itemizedlist>
</para>
</sect1>

<appendix><title>Notice légale concernant PyMecaVideo</title>
<section><title>Auteurs</title>
<para>&copy; 2007-2010, 
<author><personname><firstname>Jean-Baptiste</firstname><surname>Butet</surname></personname></author> <email>ashashiwa@gmail.com</email> : initiateur du projet PyMecaVideo
</para>
<para>&copy; 2008-2018,
<author><personname><firstname>Georges</firstname><surname>Khaznadar</surname></personname></author> <email>georgesk@debian.org</email> : contribution, empaquetage Debian
</para>
<para>&copy; 2008-2010,
<author><personname><firstname>Benoît</firstname><surname>Markey</surname></personname></author> <email>markey@free.fr</email> : vidéos d'exemple
</para>
<para>&copy; 2010,
<author><personname><firstname>Cédrick</firstname><surname>Faury</surname></personname></author> <email>cedrick.faury@laposte.net</email> : portage sous Windows, contributions
</para>
</section>
<section><title>Licence du programme</title>
<para>Le programme est sous licence libre, vous pouvez le copier, le distribuer
et le modifier, dans le respect de la licence 
<ulink url="http://www.gnu.org/licenses/quick-guide-gplv3.fr.html">GPL-V3</ulink>
</para>
</section>
<section><title>Licence des exemples</title>
<para>Les vidéos qui accompagnent le programme à titre d'exemple sont
diffusées sous la licence 
<ulink url="http://creativecommons.org/licenses/by-sa/3.0/deed.fr">
  CC-BY-SA 3.0
</ulink>.
</para>
<para>Selon les termes de cette licence, vous avez le droit de copier et 
diffuser les vidéos, ainsi que d'en faire des dérivés, et l'obligation 
de citer l'auteur original, ainsi que d'appliquer la même licence aux 
travaux dérivés.
</para>
</section>
</appendix>


</article>
